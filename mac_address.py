#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
# 
#
# Author: Tomi.Tirri@gmail.com
#

class MacAddress(object):
    def __init__(self):
        self._setLong(0)

    def getLong(self):
        return int(self.addr.replace(":",""), 16)

    def getByte(self, pos):
        return (int(self.addr.replace(":",""), 16) >> 8 * pos) & 0xFF

    def getStr(self):
        return self.addr

    @classmethod
    def fromString(cls, addr):
        obj = cls()
        obj._setAddress(addr)
        return obj

    @classmethod
    def fromLong(cls, long_value):
        obj = cls()
        obj._setLong(long_value)
        return obj

    @classmethod
    def strFromLong(cls, long_value):
        addr_string = "%012X" % (long_value)
        return cls._normalize(addr_string)

    def _setAddress(self,addr):
        self.addr = MacAddress._normalize(addr)

    def _setLong(self,long_value):
        addr_string = "%012X" % (long_value)
        self.addr = MacAddress._normalize(addr_string)

    @classmethod
    def _normalize(cls, addr):
        delimiter = "."

        if "." in addr:
            delimiter = "."
        elif ":" in addr:
            delimiter = ":"
        elif "-" in addr:
            delimiter = "-"

        m = addr.replace(delimiter, "")
        m = m.lower()

        n= ":".join(["%s%s" % (m[i], m[i+1]) for i in range(0,12,2)])

        return n



