import sys
from elp import ElpFrame
from optparse import OptionParser

#
#

class ElpApplication():
    scanreason = 1
    host = "127.0.0.1"
    port = 8552

    def __init__(self):
        self.parseCommandLine()

    def parseCommandLine(self):
        parser = OptionParser()
        parser.add_option("-r", "--reason", dest="reason",
                  help="scanreason as integer", metavar="<int>")
        parser.add_option("-s", "--server", dest="host",
                  help="server IP", metavar="<server IP>")
        parser.add_option("-p", "--port", dest="port",
                  help="ELP port", metavar="<ELP port>")

        (options, args) = parser.parse_args()
        
        if options.reason:
            self.scanreason = int(options.reason)
        if options.host:
            self.host = options.host
        if options.port:
            self.port = options.port

    def run(self):
        elpFrame = ElpFrame("42:ca:fe:ba:be:42" , self.scanreason)
        elpFrame.addScan("00:1f:9e:8d:18:61", -50, 6)
        elpFrame.addScan("00:1f:9e:8d:1c:61", -50, 1)
        elpFrame.addScan("00:19:07:c5:58:11", -50, 1)
        elpFrame.addScan("00:1f:9e:8d:20:83", -50, 1)
        elpFrame.send(self.host, self.port)

        print "One ELP frame was sent to " + self.host + ":" + str(self.port) + " with scanreason " + str(self.scanreason)
        
if __name__ == '__main__':
    app = ElpApplication()
    app.run()
