#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
# Author: Tomi.Tirri@gmail.com

import socket
import struct
import array
from mac_address import MacAddress

class ElpFrame(object):
    sequence = 0
    
    def __init__(self, mac, scanreason):
        self.mac = MacAddress.fromString(mac)
        self.version = 2
        self.type = 1
        self.timestamp = 0
        self.devicetype = 1
        self.menu = 0
        self.battery = 0
        self.messageId = 0
        self.additional = 0
        self.scanreason = scanreason
        self.channels = 65535
        self.scans = {}

    def addScan(self, mac, rssi, channel):
        self.scans[MacAddress.fromString(mac)] = [rssi, channel]

    def clearScans(self):
        self.scans = {}

    def send(self, ip, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        frame = array.array('c', '\0' * (32 + 16 * len(self.scans)))

        struct.pack_into(">ccccHHHIHBBBBBBHBBBBHH", frame, 0, "E", "L", "P", "\0", self.version, self.type, ElpFrame.sequence,\
                         self.timestamp, 16,
                         self.mac.getByte(0), self.mac.getByte(1), self.mac.getByte(2), self.mac.getByte(3), self.mac.getByte(4),\
                         self.mac.getByte(5), \
                         self.devicetype, self.menu, self.battery, self.messageId, self.additional, self.scanreason, self.channels)

        offset = 32
        for k, v in self.scans.iteritems():
            struct.pack_into(">BBBBBBhHHI", frame, offset, k.getByte(0), k.getByte(1),\
                             k.getByte(2), k.getByte(3), k.getByte(4), k.getByte(5),\
                             v[0], v[1], 0, 0)
            offset += 16
            
        struct.pack_into(">H", frame, 14, 16 + offset - 32)
        sock.sendto(frame, (ip, port))

        ElpFrame.sequence += 1
