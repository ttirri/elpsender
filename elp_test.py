import unittest
from elp import ElpFrame

class testElp(unittest.TestCase):
    def setUp(self):
        pass

    def testSend(self):
        elpFrame = ElpFrame("de:ad:be:ef:55:66", 1)
        elpFrame.addScan("11:11:11:11:11:11", -50, 6)
        elpFrame.send("127.0.0.1", 8553)

if __name__ == '__main__':
    unittest.main()
