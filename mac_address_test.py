import unittest
from mac_address import *

class testMacAddress(unittest.TestCase):
	def setUp(self):
		pass
       
	def testZeroToLong(self):
		mac_address = MacAddress.fromString("00:00:00:00:00:00")
	        self.assertEqual(mac_address.getLong(), 0)

	def test255ToLong(self):
		mac_address = MacAddress.fromString("00:00:00:00:00:FF")
	        self.assertEqual(mac_address.getLong(), 255)

	def testBigToLong(self):
		mac_address = MacAddress.fromString("FF:00:00:00:00:00")
	        self.assertEqual(mac_address.getLong(), 280375465082880L)

	def testZeroToStr(self):
		mac_address = MacAddress.fromLong(0)
	        self.assertEqual(mac_address.getStr(), "00:00:00:00:00:00")

	def test255ToStr(self):
		mac_address = MacAddress.fromLong(255)
	        self.assertEqual(mac_address.getStr(), "00:00:00:00:00:ff")

	def testBigToStr(self):
		mac_address = MacAddress.fromLong(280375465082880L)
	        self.assertEqual(mac_address.getStr(), "ff:00:00:00:00:00")

if __name__ == '__main__':
    unittest.main()
